package sqlstore_test

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store/sqlstore"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeviceRepository_Create(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters", "devices")

	s := sqlstore.New(db)

	testCluster := model.TestCluster(t)
	assert.NoError(t, s.Cluster().Create(testCluster))

	testDevice := &model.Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:    testCluster.ID,
	}

	err := s.Device().Create(testCluster.UserID, testDevice)

	assert.NoError(t, err)
	assert.NotEmpty(t, testDevice.ID)
}

func TestDeviceRepository_FindByUserID(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters", "devices")

	s := sqlstore.New(db)

	testCluster := model.TestCluster(t)
	assert.NoError(t, s.Cluster().Create(testCluster))

	s.Device().Create(testCluster.UserID, &model.Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:    testCluster.ID,
	})

	s.Device().Create(testCluster.UserID, &model.Device{
		Name:         "Тестовое устройство2",
		Description:  "Описание тестового устройства2",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:    testCluster.ID,
	})

	// find devices for existing user
	devices, err := s.Device().FindByUserIDAndClusterID(testCluster.UserID, testCluster.ID)

	assert.NoError(t, err)
	assert.NotEmpty(t, devices)

	// find devices for non existing user
	devices, err = s.Device().FindByUserIDAndClusterID(testCluster.UserID, 2)

	assert.NoError(t, err)
	assert.Empty(t, devices)
}

func TestDeviceRepository_Update(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters", "devices")

	s := sqlstore.New(db)

	testCluster := model.TestCluster(t)
	assert.NoError(t, s.Cluster().Create(testCluster))

	testDevice := &model.Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:    testCluster.ID,
	}

	assert.NoError(t, s.Device().Create(testCluster.UserID, testDevice))

	// Update valid device
	err := s.Device().Update(testCluster.UserID, testDevice.ID, &model.Device{
		Name: "Новое название устройства",
	})

	assert.NoError(t, err)
}

func TestDeviceRepository_Delete(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters", "devices")

	s := sqlstore.New(db)

	testCluster := model.TestCluster(t)
	assert.NoError(t, s.Cluster().Create(testCluster))

	testDevice := &model.Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:    testCluster.ID,
	}

	assert.NoError(t, s.Device().Create(testCluster.ID, testDevice))

	// Deleting device
	assert.NoError(t, s.Device().Delete(testCluster.UserID, testDevice.ID))

	// Assert that use has no devices
	devices, err := s.Device().FindByUserIDAndClusterID(testCluster.UserID, testCluster.ID)

	assert.NoError(t, err)
	assert.Empty(t, devices)
}
