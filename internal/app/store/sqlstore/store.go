package sqlstore

import (
	"database/sql"
	"devices-service/internal/app/store"

	_ "github.com/lib/pq" // use postgres as default driver
)

// Store represents basic entity storage
type Store struct {
	db                *sql.DB
	clusterRepository *ClusterRepository
	deviceRepository  *DeviceRepository
}

// New instantiates a new store
func New(db *sql.DB) *Store {
	return &Store{
		db: db,
	}
}

// Device returns singleton device cluster repository
func (s *Store) Cluster() store.ClusterRepository {
	if s.clusterRepository != nil {
		return s.clusterRepository
	}

	s.clusterRepository = &ClusterRepository{
		store: s,
	}

	return s.clusterRepository
}

// Device returns singleton device repository
func (s *Store) Device() store.DeviceRepository {
	if s.deviceRepository != nil {
		return s.deviceRepository
	}

	s.deviceRepository = &DeviceRepository{
		store: s,
	}

	return s.deviceRepository
}
