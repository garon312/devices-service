package sqlstore_test

import (
	"os"
	"testing"

	"github.com/BurntSushi/toml"
)

var config struct{
	DatabaseURL string `toml:"database_url"`
}

func TestMain(m *testing.M) {
	_, err := toml.DecodeFile("configs/apiserver/apiserver_test.toml", config)

	if err != nil || config.DatabaseURL == "" {
		config.DatabaseURL = "host=localhost dbname=devices_test sslmode=disable"
	}

	os.Exit(m.Run())
}
