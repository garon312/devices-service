package sqlstore

import (
	"database/sql"
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
)

type ClusterRepository struct {
	store *Store
}

// Create stores new cluster in the database
func (r *ClusterRepository) Create(c *model.Cluster) error {
	if err := c.ValidateCluster(); err != nil {
		return store.NewValidationError(err.Error())
	}

	return r.store.db.QueryRow(
		"INSERT INTO clusters (name, user_id) VALUES ($1, $2) RETURNING id",
		c.Name,
		c.UserID,
	).Scan(&c.ID)
}

// FindByUserID searches database for clusters by given user id
func (r *ClusterRepository) FindByUserID(userID int) ([]*model.Cluster, error) {
	var clusters []*model.Cluster

	rows, err := r.store.db.Query(
		"SELECT id, name, user_id FROM clusters WHERE user_id = $1",
		userID,
	)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		c := &model.Cluster{}

		if err = rows.Scan(
			&c.ID,
			&c.Name,
			&c.UserID,
		); err != nil {
			return nil, err
		}

		clusters = append(clusters, c)
	}

	err = rows.Err()

	if err != nil {
		return nil, err
	}

	return clusters, nil
}

// Update validates and updates cluster
func (r *ClusterRepository) Update(clusterID int, c *model.Cluster) error {
	dbCluster := &model.Cluster{}

	if err := r.store.db.QueryRow(
		"SELECT id, name, user_id FROM clusters WHERE id = $1",
		clusterID,
	).Scan(
		&dbCluster.ID,
		&dbCluster.Name,
		&dbCluster.UserID,
	); err != nil {
		if err == sql.ErrNoRows {
			return store.ErrRecordNotFound
		}

		return err
	}

	c.MergeWith(dbCluster)

	if err := dbCluster.ValidateCluster(); err != nil {
		return store.NewValidationError(err.Error())
	}

	if _, err := r.store.db.Exec(
		"UPDATE clusters SET name = $2, user_id = $3 WHERE id = $1 RETURNING id",
		c.ID,
		c.Name,
		c.UserID,
	); err != nil {
		return err
	}

	return nil
}

// Delete removes model from repository by its id
func (r *ClusterRepository) Delete(clusterID int) error {
	c := model.Cluster{}

	if err := r.store.db.QueryRow(
		"SELECT id FROM clusters WHERE id = $1",
		clusterID,
	).Scan(
		&c.ID,
	); err != nil {
		if err == sql.ErrNoRows {
			return store.ErrRecordNotFound
		}

		return err
	}

	_, err := r.store.db.Exec(
		"DELETE FROM clusters WHERE id = $1",
		c.ID,
	)

	if err != nil {
		return err
	}

	return nil
}
