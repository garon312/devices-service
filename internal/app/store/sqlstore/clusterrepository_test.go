package sqlstore_test

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
	"devices-service/internal/app/store/sqlstore"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClusterRepository_Create(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters")

	s := sqlstore.New(db)
	c := model.TestCluster(t)

	assert.NoError(t, s.Cluster().Create(c))
	assert.NotNil(t, c)
}

func TestClusterRepository_FindByUserID(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters")

	s := sqlstore.New(db)

	firstTestCluster := &model.Cluster{
		Name:         "Тестовый кластер устройств",
		UserID:       1,
	}

	secondTestCluster := &model.Cluster{
		Name:         "Другой тестовый кластер устройств",
		UserID:       1,
	}

	assert.NoError(t, s.Cluster().Create(firstTestCluster))
	assert.NoError(t, s.Cluster().Create(secondTestCluster))

	// find device clusters for existing user id
	clusters, err := s.Cluster().FindByUserID(firstTestCluster.UserID)

	assert.NoError(t, err)
	assert.NotEmpty(t, clusters)
	assert.Len(t, clusters, 2)

	// find device clusters for non existing user id
	clusters, err = s.Cluster().FindByUserID(9999)

	assert.NoError(t, err)
	assert.Empty(t, clusters)
}

func TestClusterRepository_Update(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters")

	s := sqlstore.New(db)

	testCluster := model.TestCluster(t)

	s.Cluster().Create(testCluster)

	// Update valid cluster
	err := s.Cluster().Update(testCluster.ID, &model.Cluster{
		Name: "Новое название кластера",
	})

	assert.NoError(t, err)
}

func TestClusterRepository_Delete(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters")

	s := sqlstore.New(db)

	firstTestCluster := &model.Cluster{
		Name:         "Тестовый кластер устройств",
		UserID:       1,
	}

	secondTestCluster := &model.Cluster{
		Name:         "Другой тестовый кластер устройств",
		UserID:       1,
	}

	assert.NoError(t, s.Cluster().Create(firstTestCluster))
	assert.NoError(t, s.Cluster().Create(secondTestCluster))

	// Delete a valid cluster with id 1
	assert.NoError(t, s.Cluster().Delete(firstTestCluster.ID))

	// Get remaining cluster for user with id 1
	clusters, err := s.Cluster().FindByUserID(secondTestCluster.UserID)

	assert.NoError(t, err)
	assert.NotEmpty(t, clusters)
	assert.Len(t, clusters, 1)

	// Try to delete non existing cluster
	err = s.Cluster().Delete(9999)

	assert.Error(t, err)
	assert.Error(t, err, store.ErrRecordNotFound.Error())
}

func TestClusterRepository_CascadeDelete(t *testing.T) {
	db, truncate := sqlstore.TestDB(t, config.DatabaseURL)
	defer truncate("clusters")

	s := sqlstore.New(db)

	c := model.TestCluster(t)
	assert.NoError(t, s.Cluster().Create(c))

	// Creating cluster's devices
	s.Device().Create(c.ID, &model.Device{
		Name: "Тестовое устройство",
		Description: "Описание тестового устройства",
		Measure: "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID: c.ID,
	})

	// Delete cluster
	assert.NoError(t, s.Cluster().Delete(c.ID))

	// Check that device is also deleted
	device, err := s.Device().FindByUserIDAndClusterID(c.UserID, c.ID)

	assert.NoError(t, err)
	assert.Len(t, device, 0)
}
