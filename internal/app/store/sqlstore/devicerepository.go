package sqlstore

import (
	"database/sql"
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"

	"github.com/lib/pq"
)

// DeviceRepository represents a repository containing devices
type DeviceRepository struct {
	store *Store
}

// Create saves the device model in the repository
func (r *DeviceRepository) Create(userID int, d *model.Device) error {
	if err := d.ValidateDevice(); err != nil {
		return store.NewValidationError(err.Error())
	}

	cluster := &model.Cluster{}

	if err := r.store.db.QueryRow(
		"SELECT user_id FROM clusters WHERE user_id = $1 AND id = $2",
		userID,
		d.ClusterID,
	).Scan(&cluster.UserID); err != nil {
		return store.ErrClusterNotFound
	}

	if err := r.store.db.QueryRow(
		"INSERT INTO devices (name, description, measure, measure_short, cluster_id) VALUES ($1, $2, $3, $4, $5) RETURNING ID",
		d.Name,
		d.Description,
		d.MeasureShort,
		d.Measure,
		d.ClusterID,
	).Scan(&d.ID); err != nil {
		switch e := err.(type) {
		case *pq.Error:
			switch e.Code {
			case "23503":
				return store.ErrClusterNotFound
			}
		default:
			return err
		}
	}

	return nil
}

// FindByIDAndUserID finds device by given id and its cluster's user id
func (r *DeviceRepository) FindByIDAndUserID(id, userID int) (*model.Device, error) {
	device := &model.Device{}

	if err := r.store.db.QueryRow(
		"SELECT d.id, d.name, d.description, d.measure, d.measure_short, d.cluster_id FROM devices d " +
			"INNER JOIN clusters c ON d.cluster_id = c.id " +
			"WHERE d.id = $1 AND c.user_id = $2",
			id,
			userID,
	).Scan(
		&device.ID,
		&device.Name,
		&device.Description,
		&device.Measure,
		&device.MeasureShort,
		&device.ClusterID,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecordNotFound
		}

		return nil, err
	}

	return device, nil
}

// FindByUserIDAndClusterID searches the device by user id
func (r *DeviceRepository) FindByUserIDAndClusterID(userID, clusterID int) ([]*model.Device, error) {
	var devices []*model.Device

	rows, err := r.store.db.Query(
		"SELECT d.id, d.name, d.description, d.measure, d.measure_short, d.cluster_id FROM devices d " +
			"INNER JOIN clusters c ON d.cluster_id = c.id " +
			"WHERE d.cluster_id = $1 AND c.user_id = $2",
		clusterID,
		userID,
	)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {

		d := &model.Device{}

		if err := rows.Scan(
			&d.ID,
			&d.Name,
			&d.Description,
			&d.Measure,
			&d.MeasureShort,
			&d.ClusterID,
		); err != nil {
			return nil, err
		}

		devices = append(devices, d)
	}

	err = rows.Err()

	if err != nil {
		return nil, err
	}

	return devices, nil
}

// Update updates existing device with given data
func (r *DeviceRepository) Update(userID int, deviceID int, d *model.Device) error {
	dbDevice := &model.Device{}

	if err := r.store.db.QueryRow(
		"SELECT d.id, d.name, d.description, d.measure, d.measure_short, d.cluster_id FROM devices d " +
			"INNER JOIN clusters c ON d.cluster_id = c.id " +
			"WHERE d.id = $1 AND c.user_id = $2",
		deviceID,
		userID,
	).Scan(
		&dbDevice.ID,
		&dbDevice.Name,
		&dbDevice.Description,
		&dbDevice.Measure,
		&dbDevice.MeasureShort,
		&dbDevice.ClusterID,
	); err != nil {
		if err == sql.ErrNoRows {
			return store.ErrRecordNotFound
		}

		return err
	}

	d.MergeWith(dbDevice)

	if err := dbDevice.ValidateDevice(); err != nil {
		return store.NewValidationError(err.Error())
	}

	if _, err := r.store.db.Exec(
		"UPDATE devices SET name = $2, description = $3, measure = $4, measure_short = $5, cluster_id = $6 WHERE id = $1",
		d.ID,
		d.Name,
		d.Description,
		d.Measure,
		d.MeasureShort,
		d.ClusterID,
	); err != nil {
		switch e := err.(type) {
		case *pq.Error:
			switch e.Code {
			case "23503":
				return store.ErrClusterNotFound
			}
		default:
			return err
		}
	}

	return nil
}

// Delete removes the device from database
func (r *DeviceRepository) Delete(userID int, deviceID int) error {
	d := &model.Device{}

	if err := r.store.db.QueryRow(
		"SELECT d.id, d.cluster_id FROM devices d " +
			"INNER JOIN clusters c ON d.cluster_id = c.id " +
			"WHERE d.id = $1 AND c.user_id = $2",
		deviceID,
		userID,
	).Scan(
		&d.ID,
		&d.ClusterID,
	); err != nil {
		if err == sql.ErrNoRows {
			return store.ErrRecordNotFound
		}

		return err
	}

	_, err := r.store.db.Exec(
		"DELETE FROM devices WHERE id = $1",
		d.ID,
	)

	if err != nil{
		return err
	}

	return nil
}
