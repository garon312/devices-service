package store

import (
	"devices-service/internal/app/model"
)

// ClusterRepository represents an interface for cluster repository
type ClusterRepository interface {
	Create(*model.Cluster) error
	FindByUserID(int) ([]*model.Cluster, error)
	Update(int, *model.Cluster) error
	Delete(int) error
}

// DeviceRepository represents an interface for device repository
type DeviceRepository interface {
	Create(int, *model.Device) error
	FindByIDAndUserID(int, int) (*model.Device, error)
	FindByUserIDAndClusterID(int, int) ([]*model.Device, error)
	Update(int, int, *model.Device) error
	Delete(int, int) error
}
