package store

import "errors"

var (
	// ErrRecordNotFound is returned when no record was found
	ErrRecordNotFound = errors.New("запись не найдена")

	// ErrClusterNotFound is sql error returned when no cluster was found by foreign key
	ErrClusterNotFound = errors.New("кластер не найден")
)

type ValidationError struct {
	text string
}

func (e *ValidationError) Error() string {
	return e.text
}

func NewValidationError(text string) *ValidationError {
	return &ValidationError{
		text,
	}
}
