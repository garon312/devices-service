package store

// Store represents main store interface containing different repositories
type Store interface {
	Cluster() ClusterRepository
	Device() DeviceRepository
}
