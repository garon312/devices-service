package teststore

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
)

// Store represents basic entity storage
type Store struct {
	clusterRepository *ClusterRepository
	deviceRepository  *DeviceRepository
}

// New instantiates a new store
func New() *Store {
	return &Store{}
}

// Cluster returns singleton test device cluster repository
func (s *Store) Cluster() store.ClusterRepository {
	if s.clusterRepository != nil {
		return s.clusterRepository
	}

	s.clusterRepository = &ClusterRepository{
		store:    s,
		clusters: make(map[int]*model.Cluster),
	}

	return s.clusterRepository
}

// Device returns singleton test device repository
func (s *Store) Device() store.DeviceRepository {
	if s.deviceRepository != nil {
		return s.deviceRepository
	}

	s.deviceRepository = &DeviceRepository{
		store:   s,
		devices: make(map[int]*model.Device),
	}

	return s.deviceRepository
}
