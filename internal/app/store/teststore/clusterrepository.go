package teststore

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
)

// ClusterRepository represents test device cluster repository
type ClusterRepository struct {
	store    *Store
	clusters map[int]*model.Cluster
}

// Create saves the device cluster model in test repository
func (r *ClusterRepository) Create(c *model.Cluster) error {
	if err := c.ValidateCluster(); err != nil {
		return store.NewValidationError(err.Error())
	}

	c.ID = len(r.clusters) + 1
	r.clusters[c.ID] = c

	return nil
}

// FindByUserID looks for device clusters matching the given userID in the test repository
func (r *ClusterRepository) FindByUserID(userID int) ([]*model.Cluster, error) {
	var result []*model.Cluster

	for _, c := range r.clusters {
		if c.UserID == userID {
			result = append(result, c)
		}
	}

	return result, nil
}

// Update in test repository replaces old cluster with new
func (r *ClusterRepository) Update(clusterID int, c *model.Cluster) error {
	oldCluster, ok := r.clusters[clusterID]

	if !ok {
		return store.ErrRecordNotFound
	}

	c.ID = oldCluster.ID
	c.UserID = oldCluster.UserID

	if err := c.ValidateCluster(); err != nil {
		return store.NewValidationError(err.Error())
	}

	r.clusters[c.ID] = c

	return nil
}

// Delete is used to delete model from repository
func (r *ClusterRepository) Delete(clusterID int) error {
	if _, ok := r.clusters[clusterID]; !ok {
		return store.ErrRecordNotFound
	}

	delete(r.clusters, clusterID)
	return nil
}