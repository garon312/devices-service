package teststore

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
)

// DeviceRepository represents test device repository
type DeviceRepository struct {
	store   *Store
	devices map[int]*model.Device
}

// Create saves the device model in test repository
func (r *DeviceRepository) Create(userID int, d *model.Device) error {
	if err := d.ValidateDevice(); err != nil {
		return store.NewValidationError(err.Error())
	}

	d.ID = len(r.devices) + 1
	r.devices[d.ID] = d

	return nil
}

// FindByIDAndUserID finds device by given id and its cluster's user id
func (r *DeviceRepository) FindByIDAndUserID(id, userID int) (*model.Device, error) {
	return nil, nil
}

// FindByUserIDAndClusterID looks for devices matching the given userID in the test repository
func (r *DeviceRepository) FindByUserIDAndClusterID(userID int, clusterID int) ([]*model.Device, error) {
	var result []*model.Device

	for _, d := range r.devices {
		if d.ClusterID == clusterID {
			result = append(result, d)
		}
	}

	return result, nil
}

// Update in test repository updates device directly
func (r *DeviceRepository) Update(userID int, deviceID int, d *model.Device) error {
	dbDevice, ok := r.devices[deviceID]

	if !ok {
		return store.ErrRecordNotFound
	}

	d.MergeWith(dbDevice)

	if err := d.ValidateDevice(); err != nil {
		return store.NewValidationError(err.Error())
	}

	r.devices[deviceID] = d

	return nil
}

// Delete removes device from test store
func (r *DeviceRepository) Delete(userID int, deviceID int) error {
	if _, ok := r.devices[deviceID]; !ok {
		return store.ErrRecordNotFound
	}

	delete(r.devices, deviceID)
	return nil
}
