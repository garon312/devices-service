package teststore_test

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store/teststore"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeviceRepository_Create(t *testing.T) {
	s := teststore.New()

	testCluster := model.TestCluster(t)
	s.Cluster().Create(testCluster)

	testDevice := &model.Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:       testCluster.ID,
	}

	err := s.Device().Create(testCluster.UserID, testDevice)

	assert.NoError(t, err)
	assert.NotEmpty(t, testDevice.ID)
}

func TestDeviceRepository_FindByUserID(t *testing.T) {
	s := teststore.New()

	testCluster := model.TestCluster(t)
	s.Cluster().Create(testCluster)

	s.Device().Create(1, &model.Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:       testCluster.ID,
	})

	s.Device().Create(1, &model.Device{
		Name:         "Тестовое устройство2",
		Description:  "Описание тестового устройства2",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:       testCluster.ID,
	})

	// find devices for existing user
	devices, err := s.Device().FindByUserIDAndClusterID(1, testCluster.ID)

	assert.NoError(t, err)
	assert.NotEmpty(t, devices)

	// find devices for non existing user
	devices, err = s.Device().FindByUserIDAndClusterID(1, 2)

	assert.NoError(t, err)
	assert.Empty(t, devices)
}
