package teststore_test

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
	"devices-service/internal/app/store/teststore"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClusterRepository_Create(t *testing.T) {
	s := teststore.New()
	d := model.TestCluster(t)

	assert.NoError(t, s.Cluster().Create(d))
	assert.NotNil(t, d)
}

func TestClusterRepository_FindByUserID(t *testing.T) {
	s := teststore.New()

	s.Cluster().Create(&model.Cluster{
		Name:         "Тестовый кластер устройств",
		UserID:       1,
	})

	s.Cluster().Create(&model.Cluster{
		Name:         "Другой тестовый кластер устройств",
		UserID:       1,
	})

	// find device clusters for existing user id
	clusters, err := s.Cluster().FindByUserID(1)

	assert.NoError(t, err)
	assert.NotEmpty(t, clusters)
	assert.Len(t, clusters, 2)

	// find device clusters for non existing user id
	clusters, err = s.Cluster().FindByUserID(2)

	assert.NoError(t, err)
	assert.Empty(t, clusters)
}

func TestClusterRepository_Delete(t *testing.T) {
	s := teststore.New()

	s.Cluster().Create(&model.Cluster{
		Name: "Тестовый кластер устройств",
		UserID:       1,
	})

	s.Cluster().Create(&model.Cluster{
		Name:         "Другой тестовый кластер устройств",
		UserID:       1,
	})

	// Delete a valid cluster with id 1
	err := s.Cluster().Delete(1)

	assert.NoError(t, err)

	// Get remaining cluster for user with id 1
	clusters, err := s.Cluster().FindByUserID(1)

	assert.NoError(t, err)
	assert.NotEmpty(t, clusters)
	assert.Len(t, clusters, 1)

	// Try to delete non existing cluster
	err = s.Cluster().Delete(123)

	assert.Error(t, err)
	assert.Error(t, err, store.ErrRecordNotFound.Error())
}

func TestClusterRepository_Update(t *testing.T) {
	s := teststore.New()

	s.Cluster().Create(&model.Cluster{
		Name: "Тестовый кластер устройств",
		UserID:       1,
	})

	// Update valid cluster
	err := s.Cluster().Update(1, &model.Cluster{
		Name: "Новое название кластера",
	})

	assert.NoError(t, err)

	invalidCluster := &model.Cluster{
		Name: "",
	}

	err = s.Cluster().Update(1, invalidCluster)

	assert.Error(t, err)
	assert.EqualError(t, err, invalidCluster.ValidateCluster().Error())
}