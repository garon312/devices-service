package model

import "testing"

// TestDevice provides basic mock device
func TestDevice(t *testing.T) *Device {
	t.Helper()

	return &Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:       1,
	}
}

// TestCluster provides basic mock device cluster
func TestCluster(t *testing.T) *Cluster {
	t.Helper()

	return &Cluster{
		Name:   "Тестовый кластер устройств",
		UserID: 1,
	}
}
