package model_test

import (
	"devices-service/internal/app/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDevice_Validate(t *testing.T) {
	testCases := []struct {
		name    string
		d       func() *model.Device
		isValid bool
	}{
		{
			name: "valid",
			d: func() *model.Device {
				return model.TestDevice(t)
			},
			isValid: true,
		},
		{
			name: "empty name",
			d: func() *model.Device {
				device := model.TestDevice(t)
				device.Name = ""

				return device
			},
			isValid: false,
		},
		{
			name: "empty measure name",
			d: func() *model.Device {
				device := model.TestDevice(t)
				device.Measure = ""

				return device
			},
			isValid: false,
		},
		{
			name: "empty measure short name",
			d: func() *model.Device {
				device := model.TestDevice(t)
				device.MeasureShort = ""

				return device
			},
			isValid: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.isValid {
				assert.NoError(t, tc.d().ValidateDevice())
			} else {
				assert.Error(t, tc.d().ValidateDevice())
			}
		})
	}
}
