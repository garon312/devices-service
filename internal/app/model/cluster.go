package model

import validation "github.com/go-ozzo/ozzo-validation"

// Cluster represents a group of device
type Cluster struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	UserID int    `json:"user_id"`
}

// ValidateCluster is used to validate cluster fields
func (c *Cluster) ValidateCluster() error {
	return validation.ValidateStruct(
		c,
		validation.Field(&c.Name, validation.Required),
		validation.Field(&c.UserID, validation.Required),
	)
}

func (c *Cluster) MergeWith(newc *Cluster) {
	if c.ID == 0 {
		c.ID = newc.ID
	}

	if c.Name == "" {
		c.Name = newc.Name
	}

	if c.UserID == 0 {
		c.UserID = newc.UserID
	}
}
