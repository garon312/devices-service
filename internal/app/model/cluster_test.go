package model_test

import (
	"devices-service/internal/app/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCluster_Validate(t *testing.T) {
	testCases := []struct {
		name    string
		d       func() *model.Cluster
		isValid bool
	}{
		{
			name: "valid",
			d: func() *model.Cluster {
				return model.TestCluster(t)
			},
			isValid: true,
		},
		{
			name: "empty name",
			d: func() *model.Cluster {
				cluster := model.TestCluster(t)
				cluster.Name = ""

				return cluster
			},
			isValid: false,
		},
		{
			name: "no user id",
			d: func() *model.Cluster {
				cluster := model.TestCluster(t)
				cluster.UserID = 0

				return cluster
			},
			isValid: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.isValid {
				assert.NoError(t, tc.d().ValidateCluster())
			} else {
				assert.Error(t, tc.d().ValidateCluster())
			}
		})
	}
}
