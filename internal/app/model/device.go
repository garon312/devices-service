package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
)

// Device represents basic device model
type Device struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	Measure      string `json:"measure"`
	MeasureShort string `json:"measure_short"`
	ClusterID    int    `json:"cluster_id"`
}

// ValidateDevice is used to validate device fields
func (d *Device) ValidateDevice() error {
	return validation.ValidateStruct(
		d,
		validation.Field(&d.Name, validation.Required),
		validation.Field(&d.Measure, validation.Required),
		validation.Field(&d.MeasureShort, validation.Required),
		validation.Field(&d.ClusterID, validation.Required),
	)
}

func (d *Device) MergeWith(newd *Device) {
	if d.ID == 0 {
		d.ID = newd.ID
	}

	if d.Name == "" {
		d.Name = newd.Name
	}

	if d.Description == "" {
		d.Description = newd.Description
	}

	if d.Measure == "" {
		d.Measure = newd.Measure
	}

	if d.MeasureShort == "" {
		d.MeasureShort = newd.MeasureShort
	}

	if d.ClusterID == 0 {
		d.ClusterID = newd.ClusterID
	}
}
