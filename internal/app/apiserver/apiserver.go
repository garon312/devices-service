package apiserver

import (
	"database/sql"
	"devices-service/internal/app/store/sqlstore"
	"net/http"

	"github.com/sirupsen/logrus"
)

// Start starts APIServer
func Start(config *Config) error {
	logger := newLogger(config.LogLevel)

	logger.Print("---Devices service---")
	logger.Print("Initializing database connection")

	db, err := newDB(config.DatabaseURL)

	if err != nil {
		return err
	}

	defer db.Close()

	logger.Print("Initializing sql store")
	store := sqlstore.New(db)

	logger.Print("Initializing api server")
	s := newServer(store, logger)

	logger.Printf("Starting server on %s", config.BindAddr)
	return http.ListenAndServe(config.BindAddr, s)
}

func newDB(databaseURL string) (*sql.DB, error) {
	db, err := sql.Open("postgres", databaseURL)

	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}

func newLogger(logLevel string) *logrus.Logger {
	var level logrus.Level

	switch logLevel {
	case "info":
		level = logrus.InfoLevel
	case "warn":
		level = logrus.WarnLevel
	case "error":
		level = logrus.ErrorLevel
	case "fatal":
		level = logrus.FatalLevel
	default:
		level = logrus.DebugLevel
	}

	logger := logrus.New()
	logger.SetLevel(level)

	return logger
}
