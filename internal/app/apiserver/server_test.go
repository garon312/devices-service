package apiserver

import (
	"bytes"
	"devices-service/internal/app/model"
	"devices-service/internal/app/store/teststore"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestServer_HandleDevicesFindByUserID(t *testing.T) {
	s := newServer(teststore.New(), nil)

	// Create test cluster
	testCluster := &model.Cluster{
		Name: "Тестовый кластер устройств",
		UserID: 1,
	}

	s.store.Cluster().Create(testCluster)

	// Create test devices
	s.store.Device().Create(testCluster.ID, &model.Device{
		Name:         "Тестовое устройство",
		Description:  "Описание тестового устройства",
		Measure:      "Энергопотребление",
		MeasureShort: "кВт/ч",
		ClusterID:    testCluster.ID,
	})

	testCases := []struct {
		name         string
		clusterID    string // url parameter is given as string
		expectedCode int
	}{
		{
			name:         "valid",
			clusterID:    "1",
			expectedCode: http.StatusOK,
		},
		{
			name:         "invalid user id",
			clusterID:    "asdasd",
			expectedCode: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/devices/%s", tc.clusterID), nil)

			s.ServeHTTP(rec, req)

			assert.Equal(t, tc.expectedCode, rec.Code)
		})
	}

}

func TestServer_HandleDevicesCreate(t *testing.T) {
	s := newServer(teststore.New(), nil)

	// Create test cluster
	testCluster := &model.Cluster{
		Name: "Тестовый кластер устройств",
		UserID: 1,
	}

	s.store.Cluster().Create(testCluster)

	testCases := []struct {
		name         string
		payload      interface{}
		expectedCode int
	}{
		{
			name: "valid",
			payload: map[string]interface{}{
				"name":          "Энергосчетчик",
				"description":   "Описание энергосчетчика",
				"measure":       "Энергопотребление",
				"measure_short": "кВт/ч",
				"cluster_id":       testCluster.ID,
			},
			expectedCode: http.StatusCreated,
		},
		{
			name:         "invalid payload",
			payload:      123,
			expectedCode: http.StatusBadRequest,
		},
		{
			name: "missing required cluster id and measures",
			payload: map[string]interface{}{
				"name":        "Энергосчетчик",
				"description": "Описание энергосчетчика",
			},
			expectedCode: http.StatusUnprocessableEntity,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			b := &bytes.Buffer{}
			json.NewEncoder(b).Encode(tc.payload)
			req, _ := http.NewRequest(http.MethodPost, "/devices", b)
			s.ServeHTTP(rec, req)

			assert.Equal(t, tc.expectedCode, rec.Code)
		})
	}
}
