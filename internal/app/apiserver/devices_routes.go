package apiserver

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func (s *server) handleDevicesCreate() http.HandlerFunc {
	type request struct {
		Name         string `json:"name"`
		Description  string `json:"description"`
		Measure      string `json:"measure"`
		MeasureShort string `json:"measure_short"`
		ClusterID    int    `json:"cluster_id"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}

		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, http.StatusBadRequest, errInvalidBody)
			return
		}

		params := mux.Vars(r)
		userID, err := strconv.Atoi(params["user_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		device := &model.Device{
			Name:         req.Name,
			Description:  req.Description,
			Measure:      req.Measure,
			MeasureShort: req.MeasureShort,
			ClusterID:    req.ClusterID,
		}

		if err := s.store.Device().Create(userID, device); err != nil {
			var ve *store.ValidationError

			if errors.As(err, &ve) {
				s.error(w, http.StatusUnprocessableEntity, ve)
				return
			}

			if errors.Is(err, store.ErrClusterNotFound) {
				s.error(w, http.StatusNotFound, err)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusCreated, device)
	}
}

func (s *server) handleDevicesFindByID() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		deviceID, err := strconv.Atoi(params["device_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		userID, err := strconv.Atoi(params["user_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		devices, err := s.store.Device().FindByIDAndUserID(deviceID, userID)

		if err != nil {
			if errors.Is(err, store.ErrClusterNotFound) {
				s.error(w, http.StatusNotFound, err)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusOK, devices)
	}
}

func (s *server) handleDevicesFindByClusterID() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		clusterID, err := strconv.Atoi(params["cluster_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		userID, err := strconv.Atoi(params["user_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		devices, err := s.store.Device().FindByUserIDAndClusterID(userID, clusterID)

		if err != nil {
			if errors.Is(err, store.ErrClusterNotFound) {
				s.error(w, http.StatusNotFound, err)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusOK, devices)
	}
}

func (s *server) handleDevicesUpdate() http.HandlerFunc {
	type request struct {
		Name         string `json:"name"`
		Description  string `json:"description"`
		Measure      string `json:"measure"`
		MeasureShort string `json:"measure_short"`
		ClusterID    int    `json:"cluster_id"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		userID, err := strconv.Atoi(params["user_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		deviceID, err := strconv.Atoi(params["device_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		req := &request{}

		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, http.StatusBadRequest, errInvalidBody)
			return
		}

		device := &model.Device{
			Name: req.Name,
			Description: req.Description,
			Measure: req.Measure,
			MeasureShort: req.MeasureShort,
			ClusterID: req.ClusterID,
		}

		if err := s.store.Device().Update(userID, deviceID, device); err != nil {
			var ve *store.ValidationError

			if errors.As(err, &ve) {
				s.error(w, http.StatusUnprocessableEntity, ve)
				return
			}

			if errors.Is(err, store.ErrClusterNotFound) {
				s.error(w, http.StatusNotFound, err)
				return
			}

			if errors.Is(err, store.ErrRecordNotFound) {
				s.error(w, http.StatusNotFound, err)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusOK, device)
	}
}

func (s *server) handleDevicesDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		deviceID, err := strconv.Atoi(params["device_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		userID, err := strconv.Atoi(params["user_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		if err := s.store.Device().Delete(userID, deviceID); err != nil {
			if err == store.ErrRecordNotFound {
				s.error(w, http.StatusNotFound, err)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusOK, nil)
	}
}
