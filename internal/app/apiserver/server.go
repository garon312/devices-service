package apiserver

import (
	"devices-service/internal/app/store"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

var (
	errInternalServerError = errors.New("internal server error")
	errInvalidBody         = errors.New("invalid body provided")
	errInvalidURLParam     = errors.New("invalid url params provided")
)

type server struct {
	router *mux.Router
	logger *logrus.Logger
	store  store.Store
}

type response struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
}

func newServer(store store.Store, logger *logrus.Logger) *server {
	s := &server{
		router: mux.NewRouter(),
		logger: logger,
		store:  store,
	}

	s.configureRouter()

	return s
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *server) configureRouter() {
	// global middleware
	s.router.Use(s.logRequest)
	s.router.Use(handlers.CORS(handlers.AllowedOrigins([]string{"*"})))

	// cluster routes
	s.router.HandleFunc("/clusters", s.handleClustersCreate()).Methods("POST")
	s.router.HandleFunc("/clusters/{user_id}", s.handleClustersFindByUserID()).Methods("GET")
	s.router.HandleFunc("/clusters/{user_id}/{cluster_id}", s.handleClustersUpdate()).Methods("PUT")
	s.router.HandleFunc("/clusters/{user_id}/{cluster_id}", s.handleClustersDelete()).Methods("DELETE")

	// devices routes
	s.router.HandleFunc("/devices/{user_id}", s.handleDevicesCreate()).Methods("POST")
	s.router.HandleFunc("/devices/{user_id}/{device_id}", s.handleDevicesFindByID()).Methods("GET")
	s.router.HandleFunc("/devices/{user_id}/cluster/{cluster_id}", s.handleDevicesFindByClusterID()).Methods("GET")
	s.router.HandleFunc("/devices/{user_id}/{device_id}", s.handleDevicesUpdate()).Methods("PUT")
	s.router.HandleFunc("/devices/{user_id}/{device_id}", s.handleDevicesDelete()).Methods("DELETE")
}

func (s *server) logRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		start := time.Now()
		rw := &responseWriter{w, http.StatusOK}
		next.ServeHTTP(rw, r)

		var level logrus.Level
		switch {
		case rw.code >= 500:
			level = logrus.ErrorLevel
		case rw.code >= 400:
			level = logrus.WarnLevel
		default:
			level = logrus.InfoLevel
		}

		logger := s.logger.WithFields(logrus.Fields{
			"request_addr": r.RemoteAddr,
			"request_time": time.Now().Sub(start),
		})

		logger.Logf(
			level,
			"%s %s %d",
			r.Method,
			r.RequestURI,
			rw.code,
		)
	})
}

func (s *server) error(w http.ResponseWriter, statusCode int, err error) {
	response := &response{
		Success: false,
		Data: map[string]string{"error": err.Error()},
	}

	s.respond(w, statusCode, response)
}

func (s *server) success(w http.ResponseWriter, statusCode int, data interface{}) {
	response := &response{
		Success: true,
		Data: data,
	}

	s.respond(w, statusCode, response)
}

func (s *server) respond(w http.ResponseWriter, statusCode int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(data)
}
