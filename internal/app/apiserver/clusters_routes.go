package apiserver

import (
	"devices-service/internal/app/model"
	"devices-service/internal/app/store"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func (s *server) handleClustersCreate() http.HandlerFunc {
	type request struct {
		Name   string `json:"name"`
		UserID int    `json:"user_id"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}

		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			s.error(w, http.StatusBadRequest, errInvalidBody)
			return
		}

		cluster := &model.Cluster{
			Name: req.Name,
			UserID: req.UserID,
		}

		if err := s.store.Cluster().Create(cluster); err != nil {
			var ve *store.ValidationError

			if errors.As(err, &ve) {
				s.error(w, http.StatusUnprocessableEntity, ve)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusCreated, cluster)
	}
}

func (s *server) handleClustersDelete() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		clusterID, err := strconv.Atoi(params["cluster_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		if err := s.store.Cluster().Delete(clusterID); err != nil {
			if err == store.ErrRecordNotFound {
				s.error(w, http.StatusNotFound, err)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusOK, nil)
	}
}

func (s *server) handleClustersUpdate() http.HandlerFunc {
	type request struct {
		Name   string `json:"name"`
		UserID int    `json:"user_id"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		clusterID, err := strconv.Atoi(params["cluster_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		req := &request{}

		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, http.StatusBadRequest, errInvalidBody)
			return
		}

		cluster := &model.Cluster{
			Name: req.Name,
			UserID: req.UserID,
		}

		if err := s.store.Cluster().Update(clusterID, cluster); err != nil {
			var ve *store.ValidationError

			if errors.As(err, &ve) {
				s.error(w, http.StatusUnprocessableEntity, ve)
				return
			}

			if errors.Is(err, store.ErrRecordNotFound) {
				s.error(w, http.StatusNotFound, err)
				return
			}

			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusOK, cluster)
	}
}

func (s *server) handleClustersFindByUserID() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		userID, err := strconv.Atoi(params["user_id"])

		if err != nil {
			s.error(w, http.StatusBadRequest, errInvalidURLParam)
			return
		}

		clusters, err := s.store.Cluster().FindByUserID(userID)

		if err != nil {
			s.logger.Error(err)
			s.error(w, http.StatusInternalServerError, errInternalServerError)
			return
		}

		s.success(w, http.StatusOK, clusters)
	}
}
