FROM golang:1.14-alpine AS build_base

WORKDIR /tmp/app

COPY . .

RUN go build -v ./cmd/apiserver

FROM alpine:3.12

WORKDIR /app/

COPY build/apiserver .
COPY --from=build_base /tmp/app/apiserver .

CMD ["/app/apiserver"]