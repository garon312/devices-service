CREATE TABLE devices (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    description TEXT DEFAULT NULL,
    measure VARCHAR NOT NULL,
    measure_short VARCHAR NOT NULL,
    user_id INTEGER NOT NULL
);